package com.teste.java.hdi.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BrokerDataDto {
	
	@NotNull
	private String code;
	
	@NotNull
	private boolean active;
	
	@NotNull
	private Double commissionRate;

}
