package com.teste.java.hdi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BrokerResponseDto {

	private String nome;
	
	private String documento;
	
	private String codigo;
	
	private String dataCriacao;
	
	private Double taxaComissao;

	private boolean ativo;
}
