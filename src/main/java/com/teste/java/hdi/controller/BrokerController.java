package com.teste.java.hdi.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.teste.java.hdi.dto.BrokerDataDto;
import com.teste.java.hdi.dto.BrokerResponseDto;
import com.teste.java.hdi.service.BrokerService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api")
@Slf4j
public class BrokerController {
	
	@Autowired
	private BrokerService brokerService;
	
	@PutMapping("/broker/{code}")
	@ApiOperation(value = "Atualiza o status do broker.")
    public ResponseEntity<BrokerDataDto> getUpdateStatusByCode(@PathVariable String code, @RequestBody @Valid BrokerDataDto dto) {
        log.info("REST request update status broker by document : {}", code);
        BrokerDataDto response = brokerService.updateStatus(code, dto);
		return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @GetMapping("/broker")
    @ApiOperation(value = "Busca informações do broker se ativo.")
    public ResponseEntity<BrokerResponseDto> getFindByDocument(@RequestParam(required = true) String documento) {
        log.info("REST request broker by document : {}", documento);
        BrokerResponseDto response = brokerService.findBrokerByDocument(documento);
		return new ResponseEntity<>(response, HttpStatus.OK);
    }

	
	

}
