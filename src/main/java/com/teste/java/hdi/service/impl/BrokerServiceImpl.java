package com.teste.java.hdi.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.teste.java.hdi.dto.BrokerDataDto;
import com.teste.java.hdi.dto.BrokerDto;
import com.teste.java.hdi.dto.BrokerResponseDto;
import com.teste.java.hdi.errors.BusinessException;
import com.teste.java.hdi.errors.NotFoundException;
import com.teste.java.hdi.service.BrokerService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BrokerServiceImpl implements BrokerService {

	@Autowired
	RestTemplate restTemplate;
	
	@Override
	public BrokerResponseDto findBrokerByDocument(String document) {
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    HttpEntity <String> entity = new HttpEntity<String>(headers);
	    
	    String url = "http://607732991ed0ae0017d6a9b0.mockapi.io/insurance/v1/broker";
	    
	    String urlTemplate = UriComponentsBuilder.fromHttpUrl(url)
	            .queryParam("document", document)
	            .encode()
	            .toUriString();
	      
	     String jsonBrokerArray = restTemplate.exchange(urlTemplate, HttpMethod.GET, entity, String.class).getBody();
	     
	     try {
			List<BrokerDto> listBroker = objectMapper.readValue(jsonBrokerArray, new TypeReference<List<BrokerDto>>(){});
			BrokerDto broker = listBroker.stream().filter(b -> b.getDocument().equals(document)).findFirst().orElseThrow(() -> new NotFoundException("Broker não encontrado"));
			
		     BrokerDataDto brokerData = requestDetailsBroker(broker.getCode());
		     
		     if(!brokerData.isActive()) {
		    	 throw new BusinessException("Broker Inativo"); 
		     }
		     
		     return BrokerResponseDto.builder().nome(broker.getName())
		    		 .documento(broker.getDocument())
		    		 .codigo(broker.getCode())
		    		 .dataCriacao(broker.getCreateDate())
		    		 .taxaComissao(brokerData.getCommissionRate())
		    		 .ativo(brokerData.isActive())
		    		 .build();

	     } catch (NotFoundException e) {
				throw new NotFoundException(e.getMessage());
	     } catch (BusinessException e) {
				throw new BusinessException(e.getMessage());
	     } catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
	     
	}

	@Override
	public BrokerDataDto updateStatus(String code, BrokerDataDto dto) {
		ObjectMapper objectMapper = new ObjectMapper();
		
		HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    
	    BrokerDataDto brokerData = requestDetailsBroker(code);
	    brokerData.setActive(dto.isActive());
	    HttpEntity<BrokerDataDto> entity = new HttpEntity<BrokerDataDto>(brokerData, headers);
	    
	    String url = "http://607732991ed0ae0017d6a9b0.mockapi.io/insurance/v1/brokerData/"+code;
	    
	    String urlTemplate = UriComponentsBuilder.fromHttpUrl(url)
	            .encode()
	            .toUriString();
	    
	    
	    String jsonBrokerData = restTemplate.exchange(urlTemplate, HttpMethod.PUT, entity, String.class).getBody();
	     
	    try {
			BrokerDataDto responseBrokerData = objectMapper.readValue(jsonBrokerData, BrokerDataDto.class);
			return responseBrokerData;
		} catch (Exception e) {
			log.info(e.getMessage());
			throw new BusinessException(e.getMessage());
		}
	}
	
	
	
	private BrokerDataDto requestDetailsBroker(String code) {
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    HttpEntity <String> entity = new HttpEntity<String>(headers);
	    
	    String url = "http://607732991ed0ae0017d6a9b0.mockapi.io/insurance/v1/brokerData";
	    
	    String urlTemplate = UriComponentsBuilder.fromHttpUrl(url)
	            .queryParam("code", code)
	            .encode()
	            .toUriString();
	      
	    String jsonBrokerDataArray = restTemplate.exchange(urlTemplate, HttpMethod.GET, entity, String.class).getBody();
	     
	    try {
			List<BrokerDataDto> listBrokerData = objectMapper.readValue(jsonBrokerDataArray, new TypeReference<List<BrokerDataDto>>(){});
			return listBrokerData.stream().filter(broker -> broker.getCode().equals(code)).findFirst().orElseThrow(() -> new Exception("Ocorreu um erro ao buscar o broker"));
			
		} catch (Exception e) {
			log.info(e.getMessage());
			throw new NotFoundException(e.getMessage());
		}
	}

}
