package com.teste.java.hdi.service;

import com.teste.java.hdi.dto.BrokerDataDto;
import com.teste.java.hdi.dto.BrokerResponseDto;

public interface BrokerService {

	public BrokerResponseDto findBrokerByDocument(String document);
	
	public BrokerDataDto updateStatus(String code, BrokerDataDto data);
	

}
