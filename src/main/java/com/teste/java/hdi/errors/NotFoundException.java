package com.teste.java.hdi.errors;


public class NotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -180537796317276665L;

	public NotFoundException(String msg) {
		super(msg);
	}
}
