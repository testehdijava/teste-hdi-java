package com.teste.java.hdi.errors;

public class BusinessException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8291858659654546782L;

	public BusinessException(String msg) {
		super(msg);
	}
}
