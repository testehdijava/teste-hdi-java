package com.teste.java.hdi.errors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class BusinessExceptionHandler {

    @ResponseBody
	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	ResponseEntity<ApiError> notFoundHandler(NotFoundException ex) {
		return new ResponseEntity<>(new ApiError(ex.getMessage()), HttpStatus.NOT_FOUND);
	}
    
    @ResponseBody
	@ExceptionHandler(BusinessException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	ResponseEntity<ApiError> badRequestHandler(BusinessException ex) {
		return new ResponseEntity<>(new ApiError(ex.getMessage()), HttpStatus.BAD_REQUEST);
	}
    
    @ResponseBody
	@ExceptionHandler(MissingServletRequestParameterException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	ResponseEntity<ApiError> badRequestHandler(MissingServletRequestParameterException ex) {
		return new ResponseEntity<>(new ApiError("Parametro " + ex.getParameterName() + " obrigatório"), HttpStatus.BAD_REQUEST);
	}
    
    

   
}